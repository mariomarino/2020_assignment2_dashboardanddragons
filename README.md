# 2020_assignment2_DashboardAndDragons

FantasyBoard è una dashboard per semplificare la stesura e la consultazione di note nell'ambito dei giochi di ruolo.

Members
-------

* Mario Marino 829707
* Pietro Tropeano 829757

Istruzioni
----------

Il file da consultare è Plan.pdf

All'interno di Plan.pdf sono stati uniti i diversi pdf realizzati.

Per maggiori informazioni sul dominio di appartenenza del progetto è possibile consultare Intro_problem.pdf